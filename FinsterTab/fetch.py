import pandas_datareader.data as dr
import pandas as pd
from datetime import datetime
import sqlalchemy as sal
from functools import reduce


class Fetch:
    def __init__(self, fetch_type):
        self.ft = fetch_type

    def sql_engine(self):
        # CHANGE REQUEST CR-1: def sql_engine(self, conn_str)
        """
        Create SQLAlchemy Engine for SQL SERVER 2017
        Use msSQL and pyODBC library to create the connection
        Use SQL Server Native Client 11.0 Driver

        :return engine: creates connection to SQL Server
        """
        # CR-1
        # NEED TO remove conn_str = " <CONTENTS> " and store in TextFile.  Pass as parameter to sql_engine() from calls.
        conn_str = 'mssql+pyodbc://sa:Gmfintech18@localhost:1433/GMFSP_db?driver=SQL+Server+Native+Client+11.0'
        engine = sal.create_engine(conn_str)
        return engine

    def get_datasources(self, engine, table_name):
        """
        Method to query SQL for the ticker symbols of the financial data sources
        Use pandas read_sql_query function to pass 'query'
        Use sqlAlchemy engine to create connection

        :param engine: provides connection to SQL Server
        :param table_name: provides the table name of the Financial Data Sources (ticker symbols)
        :return s: pandas dataframe object containing the ticker symbols
        """
        query = 'SELECT dataSource FROM %s' % table_name
        s = pd.read_sql_query(query, engine)
        return s

    def get_data(self, engine, sources):
        """
        Get data related to the financial ticker symbol
        IF fetch_type is 'forecast':
            Create queries for each Financial Data table in GMFSP_db Database
            Merge all records on tradeDate for each ticker
            Return 2D Pandas Dataframe containing all data indexed by tradeDate
        ELSE IF fetch_type is 'strategy':
            Fetch data from IEX Trading data source using ticker symbols from dbo.FinDataSources table
            Create dbo.<ticker> tables for each ticker symbol (i.e. dbo.SPY)
            Format index and column names
            Send to GMFSP_db Database
            Return 2D Pandas Datarame containing the last 5yrs of financial instruments market data
        ELSE no fetch_type declared:
            Print Error

        :return fin_data: pandas dataframe object containing the financial data for each ticker based on fetch_type
        """
        # fin_data will be a 2D Pandas Dataframe
        fin_data = []
        # n is the counter for the sources ticker symbols
        n = 0

        # Check fetch_type (forecast or strategy)
        if self.ft == 'forecast':
            # Cycle through each ticker symbol
            while n < len(sources.index):
                # Create variable names for each ticker table.
                # Used to query for all financial data related to ticker symbol stored in GMFSP_db.
                ticker = sources.iat[n, sources.columns.get_loc('dataSource')]
                # Ticker Table (i.e. dbo.PFE)
                ticker = 'dbo.' + ticker
                # Trading Strategy Table (i.e. dbo.PFE_TS)
                ticker_ts = ticker + '_TS'
                # Trading Strategy Predictions Table (i.e. dbo.PFE_Pred)
                ticker_pred = ticker + '_Pred'
                # Secondary Trading Strategy Predictions Table (i.e. dbo.PFE_Pred2)
                ticker_pred2 = ticker + '_Pred2'
                # Trading Action Table (i.e. dbo.PFE_Act)
                ticker_act = ticker + '_Act'

                # Preserve Ordering of Data ( ORDER BY tradeDate ASC )
                qt = "SELECT * FROM %s ORDER BY tradeDate ASC" % ticker
                qts = "SELECT * FROM %s ORDER BY tradeDate ASC" % ticker_ts
                qtp = "SELECT * FROM %s ORDER BY tradeDate ASC" % ticker_pred
                qtp2 = "SELECT * FROM %s ORDER BY tradeDate ASC" % ticker_pred2
                qta = "SELECT * FROM %s ORDER BY tradeDate ASC" % ticker_act

                # Pandas read_to_sql_query function returns dataframe of table
                t = pd.read_sql_query(qt, engine)
                t_ts = pd.read_sql_query(qts, engine)
                t_p = pd.read_sql_query(qtp, engine)
                t_p2 = pd.read_sql_query(qtp2, engine)
                t_a = pd.read_sql_query(qta, engine)

                # TEST: Verify order of data (tradeDate ASC)
                # t.to_csv('ticker.csv')

                # Drop unnecessary (repetitive) columns by label (column name)
                t_p.drop(labels='closePrice', axis='columns', inplace=True)
                t_p2.drop(labels=['closePrice', 'sTrough', 'sPeak', 'momentumA', 'momentumB'],
                          axis='columns', inplace=True)
                t_a.drop(labels='closePrice', axis='columns', inplace=True)

                # LAMBDA Function: Merge left to right in t_df on tradeDate
                t_df = [t, t_ts, t_p, t_p2, t_a]
                f_final = reduce(lambda left, right: pd.merge(left, right, on='tradeDate'), t_df)

                # Append pandas dataframe to fin_data Array
                # Each ticker symbol will be its own entry, creating a 2D Array
                # i.e. fin_data[<ticker>][<tradeDate>]
                fin_data.append(f_final)

                # Advance source ticker counter
                n = n + 1
            # Return fin_data 2D Array containing all financial data and related computations
            return fin_data
        # Check fetch_type (forecast or strategy)
        elif self.ft == 'strategy':
            # Date Variables
            now = datetime.now()
            start = datetime(2013, 1, 1)
            end = now.strftime("%Y-%m-%d")

            # Cycle through each ticker symbol
            while n < len(sources.index):
                # Fetch data from IEX Trading
                # Use ticker symbol in dataSources table at position 'n' with start and end date for the time interval
                fin_data.append(dr.DataReader(sources.iat[n, sources.columns.get_loc('dataSource')], 'iex', start, end))
                # Change index and column names to avoid issue with SQL Server keywords
                fin_data[n].index.name = 'tradeDate'
                fin_data[n].rename(columns={'open': 'openPrice', 'close': 'closePrice'}, inplace=True)
                # Send to SQL use ticker symbol in dataSources table at position 'n' to name the new table.
                ticker_table = sources.iat[n, sources.columns.get_loc('dataSource')]
                # Ensure order of data is tradeDate
                fin_data[n].sort_values(by='tradeDate')
                # Send ticker financial data to SQL Server (dbo.<Ticker> Table)
                # Replace if it exists and use tradeDate as the index
                fin_data[n].to_sql(ticker_table, engine, if_exists='replace', index_label='tradeDate',
                                   dtype={'tradeDate': sal.Date, 'openPrice': sal.FLOAT, 'high': sal.FLOAT,
                                          'low': sal.FLOAT, 'closePrice': sal.FLOAT, 'volume': sal.FLOAT})

                # TEST: Verify order of data (tradeDate ASC)
                # fin_data[n].to_csv('Data.csv')

                # Advance source ticker counter
                n = n + 1
            # Return fin_data 2D Array containing all financial data and related computations
            return fin_data
        # Else no fetch_type is declared throw error
        else:
            e = 'Error: No fetch type defined for financial data'
            print(e)
            return e
