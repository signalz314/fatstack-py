import pytest
from mock import patch

try:
    from FinsterTab.fetch import Fetch
except ImportError:
    pass
try:
    from FinsterTab.forecast import Forecast
except ImportError:
    pass
try:
    from FinsterTab.executeSP import ExecuteSP
except ImportError:
    pass

# TEST_CONN_STR = 'mssql+pyodbc://sa:Gmfintech18@localhost:1433/GMFSP_db?driver=SQL+Server+Native+Client+11.0'
# TEST_DB = 'dbo.FinTest'

# SET UP SQL Engines
strategy = Fetch('strategy')
forecast = Fetch('forecast')


class TestDB:

    def __init__(self):
        self.strgy_eng = strategy.sql_engine()
        self.strgy_source = strategy.get_datasources(self.strgy_eng, 'dbo.FinDataSources')
        self.strgy_data = strategy.get_data(self.strgy_eng, self.strgy_source)
        self.fcst_eng = forecast.sql_engine()
        self.fcst_source = forecast.get_datasources(self.fcst_eng, 'dbo.FinDataSources')
        self.fcst_data = forecast.get_data(self.fcst_eng, self.fcst_data)

    def test_database_creation(self):
        """
        Method that verifies the when the database only has dbo.FinDataSources Table,
            the others are created accordingly and result in producing dbo.<ticker>_Forecast Tables.

        :return verified: Boolean, verified complete( true ? false )
        """
        n = 0
        while n < len(self.fcst_source.index):
            # Create variable names for each ticker table.
            # Used to query for all financial data related to ticker symbol stored in GMFSP_db.
            ticker = self.fcst_source.iat[n, self.fcst_source.columns.get_loc('dataSource')]
            ticker_ts = ticker + '_TS'

            # Check if Trading Strategies table exists
            if self.fcst_eng.dialect.has_table(self.fcst_eng, ticker_ts):
                return False
            else:
                n = n + 1

        return True

