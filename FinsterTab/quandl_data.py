from FinsterTab.fetch import Fetch
from FinsterTab.tradeStrategies import TradingStrategies
from FinsterTab.forecast import Forecast
from FinsterTab.executeSP import ExecuteSP
from FinsterTab.tradeSim import TradeSim


# Declare fetch_type strategy to fetch historical financial market data
fetch_type1 = 'strategy'
f1 = Fetch(fetch_type1)
t = TradingStrategies(f1)
t.compute_ts()

# Creates <ticker>_Pred, <ticker>_Pred2, and <ticker>_Act Tables
e = ExecuteSP()
e.exec_sp()

# Creates <ticker>_Forecast Table
# Initialize Fetch Class and pass to Forecast as f2
fetch_type2 = 'forecast'
f2 = Fetch(fetch_type2)
s = Forecast(f2)

# Check if <ticker> tables already exist
# If true: Execute forecast
# Else false: Re-Run process
if Forecast.check_db(s):
    fs = Forecast.compute_forecast(s)
    sim = TradeSim(f2)
    trade_sim = TradeSim.trading_simulator(sim)
else:
    # Declare fetch_type strategy to fetch historical financial market data
    fetch_type1 = 'strategy'
    f1 = Fetch(fetch_type1)
    t = TradingStrategies(f1)
    t.compute_ts()

    # Creates <ticker>_Pred, <ticker>_Pred2, and <ticker>_Act Tables
    e = ExecuteSP()
    e.exec_sp()

    # Creates <ticker>_Forecast Table
    # Creates <ticker>_Forecast Table
    fetch_type2 = 'forecast'
    f2 = Fetch(fetch_type2)
    s = Forecast(f2)
    fs = Forecast.compute_forecast(s)
    sim = TradeSim(f2)
    trade_sim = TradeSim.trading_simulator(sim)
