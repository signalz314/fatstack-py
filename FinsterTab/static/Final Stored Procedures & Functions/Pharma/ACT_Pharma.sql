USE [GMFSP_db]
GO
/****** Object:  StoredProcedure [dbo].[sp_CMA_FRL_ACT]    Script Date: 12/10/2018 10:32:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-----------------------------------------------
-- Author: Chris Del Duco
-- Author: Mohammad Moubadder
--
--			STORED PROCEDURE:
--			 sp_CMA_FRL_ACT
-----------------------------------------------

ALTER PROCEDURE [dbo].[sp_CMA_FRL_ACT]
AS
BEGIN
-----------------------------------------------
-- Create date: 11/1/2018
-- Description:	
---------------------------------------------------
-- FINANCIAL INSTRUMENT:	S&P 500 ETF Trust (SPY)
-- BUY SELL HOLD:			CASE LOGIC
-- CREATES dbo.SPY_Act from SPY_Pred
---------------------------------------------------
SET NOCOUNT ON;

IF EXISTS (SELECT * 
	FROM GMFSP_db.INFORMATION_SCHEMA.TABLES   
	WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'SPY_Act')
Drop table dbo.SPY_Act

--select * from dbo.SPY_Act
--truncate table dbo.SPY_Act

SELECT p.tradeDate, p.closePrice,
	(CASE WHEN lCMA is NULL
		THEN 3
		ELSE
		-- Yesterday's wCMA is higher than lCMA and approaches the lCMA "GOLDEN CROSS" or Stock is lower than expected
		CASE WHEN 
			-- Yesterdays wCMA is Higher than lCMA, but not Drastically
			BUYweekApproach > 1.015 and BUYweekApproach < 1.065
			-- Todays wCMA is closer than yesterday
			and week_Long >= 1.015 
			-- Yesterdays closePrice 5 days ago is higher than todays
			and p.momentumA < .98
			THEN 1
			ELSE
			-- Yesterday's wCMA is higher than sCMA and is approaching the "DEATH CROSS" or Stock is Inflated, so CASH IN
			CASE WHEN 
			-- Yesterday's wCMA is Higher than today's sCMA
			SELLweekApproach > 1.006
			-- and Todays wCMA is closer than yesterday
			and week_Short >= .95
			-- and Yesterdays sCMA is higher than todays
			and (LAG(sCMA, 3) OVER (ORDER BY ts.tradeDate) - sCMA) > 0 
			THEN 0
				ELSE 3 --  Too Volatile
			END
		END
	END) AS CMATradeAction,

(Case when dbo.fn_Get_TradeAction(p.closePrice,highFRLLineLong,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	OR dbo.fn_Get_TradeAction(p.closePrice,medFRLLineLong,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	or dbo.fn_Get_TradeAction(p.closePrice,lowFRLLineLong,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
															
	THEN 4
	ELSE
	CASE WHEN (p.tradeDate <= DATEADD(month, -6, GETDATE())) -- Date is older than 6Months
		THEN
		CASE WHEN p.closePrice >= 100.00 -- Different Ratios for Larger $ Amount
			THEN
			CASE WHEN 
			p.closePrice <= (lowFRLLineLong * .95)
			and p.momentumA < .947
				THEN 1
				ELSE
				CASE WHEN 
				p.closePrice >= (highFRLLineLong * .93)
				and p.momentumA > .955
				and (LAG(p.ActualChange, 1) OVER (ORDER BY p.tradeDate) - p.ActualChange) < 0.1
					THEN 0
					ELSE 4
				END
			END
		ELSE -- closePrice < 100.00
			CASE WHEN 
			p.closePrice <= (lowFRLLineLong * .94)
			and p.momentumA < .99
				THEN 1
				ELSE
				CASE WHEN 
				p.closePrice >= (highFRLLineLong * .90)
				and p.momentumA > 1.0
				and (LAG(p.ActualChange, 1) OVER (ORDER BY p.tradeDate) - p.ActualChange) < 0.1
					THEN 0
					ELSE 4
				END
			END
		END -- END OF CASE FOR DOLLAR $$ AMOUNT
	ELSE -- tradeDate is More Recent than 6Months
		CASE WHEN p.closePrice > 100.00
			THEN
			CASE WHEN 
			p.closePrice <= (lowFRLLineLong * .96)
			and p.momentumA < .947
				THEN 1
				ELSE
				CASE WHEN 
				p.closePrice >= (highFRLLineLong * .96)
				and p.momentumA > .96
				and (LAG(p.ActualChange, 1) OVER (ORDER BY p.tradeDate) - p.ActualChange) < 0.1
					THEN 0
					ELSE 4
				END
			END
		ELSE -- closePrice is < 100.00
			CASE WHEN 
			p.closePrice <= (medFRLLineLong * .98)
			and p.momentumA < .97
				THEN 1
				ELSE
				CASE WHEN 
				p.closePrice >= (HighFRLLineLong * .93)
				and p.momentumA > 1.02
				and (LAG(p.ActualChange, 1) OVER (ORDER BY p.tradeDate) - p.ActualChange) < 0.1
					THEN 0
					ELSE 4
				END
			END
		END -- END OF CASE FOR DOLLAR $$ AMOUNT
	END -- END OF CASE FOR DATE (6Months or Older)
END) AS FRLTradeAction,

-----------------------------------------------------
--1000 day
-----------------------------------------------------
(Case when dbo.fn_Get_TradeAction(p.closePrice,highFRLLineOneK,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	OR dbo.fn_Get_TradeAction(p.closePrice,medFRLLineOneK,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	or dbo.fn_Get_TradeAction(p.closePrice,lowFRLLineOneK,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	THEN 4
	ELSE
		CASE WHEN (p.tradeDate <= DATEADD(month, -6, GETDATE())) -- OLDER tradeDate than 6Months Ago
			THEN 
			CASE WHEN p.closePrice > 100.00 -- closePrice is Greater than 100.00
				THEN
				CASE WHEN p.closePrice <= (highFRLLineOneK * 1.01)
				and p.closePrice >= (highFRLLineOneK * .95)
					THEN 1
					ELSE
					CASE WHEN
					-- closePrice is higher than FRL, but not drastically
					p.closePrice >= (highFRLLineOneK * 1.005)
					and p.closePrice < (highFRLLineOneK * 1.04)
						THEN 0
						ELSE 4
					END
				END
			ELSE -- ELSE $$$$ < 100.00
			CASE WHEN p.closePrice <= (medFRLLineOneK * .95)
			and p.momentumA < 1.009
				THEN 1
				ELSE
				CASE WHEN
				-- closePrice is higher than FRL, but not drastically
				p.closePrice >= (highFRLLineOneK * .925)
				and p.momentumA >1.02
					THEN 0
					ELSE 4
				END
			END
		END
	ELSE -- ELSE NEWER than 6Mos
	CASE WHEN p.closeprice > 100.00
		THEN
		CASE WHEN p.closePrice <= (highFRLLineOneK * 1.03)
			and p.closePrice >= (highFRLLineOneK * 1.005)
				THEN 1
				ELSE
				CASE WHEN
				-- closePrice is higher than FRL, but not drastically
				p.closePrice >= (highFRLLineOneK * 1.075)
				and p.closePrice < (highFRLLineOneK * 1.025)
					THEN 0
					ELSE 4
				END
			END
		ELSE -- ELSE $$$$ < 100.00
			CASE WHEN 
			p.closePrice <= (medFRLLineOneK * .975)
			and p.momentumA < 1.0
			THEN 1
				ELSE
				CASE WHEN
				-- closePrice is Trending higher than FRL, but not drastically
				p.closePrice >= (highFRLLineOneK * .95)
				and p.momentumA > 1.04
					THEN 0
					ELSE 4
				END
			END
		END -- END OF DOLLAR $$$$
	END -- END OF DATE
END) AS FRL_1K_TradeAction

INTO dbo.SPY_Act
FROM dbo.SPY_Pred as p
JOIN dbo.SPY_TS as ts on ts.tradeDate = p.tradeDate
JOIN dbo.SPY_Pred2 as p2 on p2.tradeDate = p.tradeDate
ORDER BY p.tradeDate, p2.tradeDate, ts.tradeDate ASC


--------------------------- update fib columns SPY------------------
DECLARE @fibExtLowNeg Float
DECLARE @fibExtHighNeg Float
DECLARE @fibExtLowPos Float
DECLARE @fibExtHighPos Float

SELECT TOP 1 @fibExtLowNeg = fibExtLowNeg
    , @fibExtHighNeg = [fibExtHighNeg]
    , @fibExtLowPos = [fibExtLowPos] 
    ,@fibExtHighPos = [fibExtHighPos]
FROM dbo.SPY_Pred2 ORDER BY tradeDate DESC
UPDATE dbo.SPY_Pred2 SET [fibExtLowNeg] = @fibExtLowNeg , [fibExtHighNeg] = @fibExtHighNeg ,  fibExtHighPos = NULL, fibExtLowPos = NULL
--------------------------- update fib columns SPY------------------

	
-----------------------------------------------
-- FINANCIAL INSTRUMENT:	S&P Pharm ETF (XPH)
-- BUY SELL HOLD:			CASE LOGIC
-- CREATES dbo.XPH_Act from XPH_Pred
-----------------------------------------------
IF EXISTS (SELECT * 
	FROM GMFSP_db.INFORMATION_SCHEMA.TABLES   
	WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'XPH_Act')
Drop table dbo.XPH_Act

SELECT p.tradeDate, p.closePrice,
		(CASE WHEN lCMA is NULL
			THEN 3
			ELSE
			-- Yesterday's wCMA is higher than lCMA and approaches the lCMA "GOLDEN CROSS" or Stock is lower than expected
			CASE WHEN
			-- Yesterdays wCMA is Higher than lCMA, but not Drastically
			BUYweekApproach > 1.015 and BUYweekApproach < 1.065
			-- Todays wCMA is closer than yesterday
			and week_Long >= 1.015 
			-- Yesterdays closePrice 5 days ago is higher than todays
			and p.momentumA < .98
			THEN 1
			ELSE
			-- Yesterday's wCMA is higher than sCMA and is approaching the "DEATH CROSS" or Stock is Inflated, so CASH IN
			CASE WHEN 
			-- Yesterday's wCMA is Higher than today's sCMA
			SELLweekApproach > 1.006
			-- and Todays wCMA is closer than yesterday
			and week_Short >= 1.01
			-- and Yesterdays sCMA is higher than todays
			and (LAG(sCMA, 3) OVER (ORDER BY ts.tradeDate) - sCMA) > 0 
					THEN 0
					ELSE 3 --  Too Volatile
				END
			END
		END) AS CMATradeAction,
			
	(Case when dbo.fn_Get_TradeAction(p.closePrice,highFRLLineLong,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	OR dbo.fn_Get_TradeAction(p.closePrice,medFRLLineLong,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	or dbo.fn_Get_TradeAction(p.closePrice,lowFRLLineLong,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
															
	THEN 4
	ELSE
	CASE WHEN (p.tradeDate <= DATEADD(month, -6, GETDATE())) -- Date is older than 6Months
		THEN
		CASE WHEN p.closePrice >= 100.00 -- Different Ratios for Larger $ Amount
			THEN
			CASE WHEN 
			p.closePrice <= (lowFRLLineLong * .95)
			and p.momentumA < .947
				THEN 1
				ELSE
				CASE WHEN 
				p.closePrice >= (highFRLLineLong * .93)
				and p.momentumA > .955
				and (LAG(p.ActualChange, 1) OVER (ORDER BY p.tradeDate) - p.ActualChange) < 0.1
					THEN 0
					ELSE 4
				END
			END
		ELSE -- closePrice < 100.00
			CASE WHEN 
			p.closePrice <= (lowFRLLineLong * .94)
			and p.momentumA < .99
				THEN 1
				ELSE
				CASE WHEN 
				p.closePrice >= (highFRLLineLong * .90)
				and p.momentumA > 1.0
				and (LAG(p.ActualChange, 1) OVER (ORDER BY p.tradeDate) - p.ActualChange) < 0.1
					THEN 0
					ELSE 4
				END
			END
		END -- END OF CASE FOR DOLLAR $$ AMOUNT
	ELSE -- tradeDate is More Recent than 6Months
		CASE WHEN p.closePrice > 100.00
			THEN
			CASE WHEN 
			p.closePrice <= (lowFRLLineLong * .96)
			and p.momentumA < .947
				THEN 1
				ELSE
				CASE WHEN 
				p.closePrice >= (highFRLLineLong * .96)
				and p.momentumA > .96
				and (LAG(p.ActualChange, 1) OVER (ORDER BY p.tradeDate) - p.ActualChange) < 0.1
					THEN 0
					ELSE 4
				END
			END
		ELSE -- closePrice is < 100.00
			CASE WHEN 
			p.closePrice <= (medFRLLineLong * .98)
			and p.momentumA < .97
				THEN 1
				ELSE
				CASE WHEN 
				p.closePrice >= (HighFRLLineLong * .93)
				and p.momentumA > 1.02
				and (LAG(p.ActualChange, 1) OVER (ORDER BY p.tradeDate) - p.ActualChange) < 0.1
					THEN 0
					ELSE 4
				END
			END
		END -- END OF CASE FOR DOLLAR $$ AMOUNT
	END -- END OF CASE FOR DATE (6Months or Older)
END) AS FRLTradeAction,

-----------------------------------------------------
--1000 day
-----------------------------------------------------
(Case when dbo.fn_Get_TradeAction(p.closePrice,highFRLLineOneK,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	OR dbo.fn_Get_TradeAction(p.closePrice,medFRLLineOneK,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	or dbo.fn_Get_TradeAction(p.closePrice,lowFRLLineOneK,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	THEN 4
	ELSE
		CASE WHEN (p.tradeDate <= DATEADD(month, -6, GETDATE())) -- OLDER tradeDate than 6Months Ago
			THEN 
			CASE WHEN p.closePrice > 100.00 -- closePrice is Greater than 100.00
				THEN
				CASE WHEN p.closePrice <= (highFRLLineOneK * 1.01)
				and p.closePrice >= (highFRLLineOneK * .95)
					THEN 1
					ELSE
					CASE WHEN
					-- closePrice is higher than FRL, but not drastically
					p.closePrice >= (highFRLLineOneK * 1.005)
					and p.closePrice < (highFRLLineOneK * 1.04)
						THEN 0
						ELSE 4
					END
				END
			ELSE -- ELSE $$$$ < 100.00
			CASE WHEN p.closePrice <= (medFRLLineOneK * .95)
			and p.momentumA < 1.009
				THEN 1
				ELSE
				CASE WHEN
				-- closePrice is higher than FRL, but not drastically
				p.closePrice >= (highFRLLineOneK * .925)
				and p.momentumA >1.02
					THEN 0
					ELSE 4
				END
			END
		END
	ELSE -- ELSE NEWER than 6Mos
	CASE WHEN p.closeprice > 100.00
		THEN
		CASE WHEN p.closePrice <= (highFRLLineOneK * 1.03)
			and p.closePrice >= (highFRLLineOneK * 1.005)
				THEN 1
				ELSE
				CASE WHEN
				-- closePrice is higher than FRL, but not drastically
				p.closePrice >= (highFRLLineOneK * 1.075)
				and p.closePrice < (highFRLLineOneK * 1.025)
					THEN 0
					ELSE 4
				END
			END
		ELSE -- ELSE $$$$ < 100.00
			CASE WHEN 
			p.closePrice <= (medFRLLineOneK * .975)
			and p.momentumA < 1.0
			THEN 1
				ELSE
				CASE WHEN
				-- closePrice is Trending higher than FRL, but not drastically
				p.closePrice >= (highFRLLineOneK * .95)
				and p.momentumA > 1.04
					THEN 0
					ELSE 4
				END
			END
		END -- END OF DOLLAR $$$$
	END -- END OF DATE
END) AS FRL_1K_TradeAction

INTO dbo.XPH_Act
FROM dbo.XPH_Pred as p
JOIN dbo.XPH_TS as ts on ts.tradeDate = p.tradeDate
JOIN dbo.XPH_Pred2 as p2 on p2.tradeDate = p.tradeDate
ORDER BY p.tradeDate, p2.tradeDate, ts.tradeDate ASC

	
--------------------------- update fib columns XPH------------------
SELECT TOP 1 @fibExtLowNeg = fibExtLowNeg
    , @fibExtHighNeg = [fibExtHighNeg]
    , @fibExtLowPos = [fibExtLowPos] 
    ,@fibExtHighPos = [fibExtHighPos]
FROM dbo.XPH_Pred2 ORDER BY tradeDate DESC
UPDATE dbo.XPH_Pred2 SET [fibExtLowNeg] = @fibExtLowNeg , [fibExtHighNeg] = @fibExtHighNeg ,  fibExtHighPos = NULL, fibExtLowPos = NULL
--------------------------- update fib columns XPH------------------

----------------------------------------
-- FINANCIAL INSTRUMENT:	PFIZER (PFE)
-- BUY SELL HOLD:			CASE LOGIC
-- CREATES dbo.PFE_Act from PFE_Pred
----------------------------------------
IF EXISTS (SELECT * 
	FROM GMFSP_db.INFORMATION_SCHEMA.TABLES   
	WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'PFE_Act')
drop table dbo.PFE_Act

SELECT p.tradeDate, p.closePrice,
	(CASE WHEN lCMA is NULL
		THEN 3
		ELSE
		-- Yesterday's wCMA is higher than lCMA and approaches the lCMA "GOLDEN CROSS" or Stock is lower than expected
		CASE WHEN 
			-- Yesterdays wCMA is Higher than lCMA, but not Drastically
			BUYweekApproach > 1.015 and BUYweekApproach < 1.065
			-- Todays wCMA is closer than yesterday
			and week_Long >= 1.015 
			-- Yesterdays closePrice 5 days ago is higher than todays
			and p.momentumA < .98
			THEN 1
			ELSE
			-- Yesterday's wCMA is higher than sCMA and is approaching the "DEATH CROSS" or Stock is Inflated, so CASH IN
			CASE WHEN 
			-- Yesterday's wCMA is Higher than today's sCMA
			SELLweekApproach > 1.006
			-- and Todays wCMA is closer than yesterday
			and week_Short >= 1.01
			-- and Yesterdays sCMA is higher than todays
			and (LAG(sCMA, 3) OVER (ORDER BY ts.tradeDate) - sCMA) > 0 
				THEN 0
				ELSE 3 --  Too Volatile
			END
		END
	END) AS CMATradeAction,
		
	(Case when dbo.fn_Get_TradeAction(p.closePrice,highFRLLineLong,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	OR dbo.fn_Get_TradeAction(p.closePrice,medFRLLineLong,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	or dbo.fn_Get_TradeAction(p.closePrice,lowFRLLineLong,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
															
	THEN 4
	ELSE
	CASE WHEN (p.tradeDate <= DATEADD(month, -6, GETDATE())) -- Date is older than 6Months
		THEN
		CASE WHEN p.closePrice >= 100.00 -- Different Ratios for Larger $ Amount
			THEN
			CASE WHEN 
			p.closePrice <= (lowFRLLineLong * .95)
			and p.momentumA < .947
				THEN 1
				ELSE
				CASE WHEN 
				p.closePrice >= (highFRLLineLong * .93)
				and p.momentumA > .955
				and (LAG(p.ActualChange, 1) OVER (ORDER BY p.tradeDate) - p.ActualChange) < 0.1
					THEN 0
					ELSE 4
				END
			END
		ELSE -- closePrice < 100.00
			CASE WHEN 
			p.closePrice <= (lowFRLLineLong * .94)
			and p.momentumA < .99
				THEN 1
				ELSE
				CASE WHEN 
				p.closePrice >= (highFRLLineLong * .90)
				and p.momentumA > 1.0
				and (LAG(p.ActualChange, 1) OVER (ORDER BY p.tradeDate) - p.ActualChange) < 0.1
					THEN 0
					ELSE 4
				END
			END
		END -- END OF CASE FOR DOLLAR $$ AMOUNT
	ELSE -- tradeDate is More Recent than 6Months
		CASE WHEN p.closePrice > 100.00
			THEN
			CASE WHEN 
			p.closePrice <= (lowFRLLineLong * .96)
			and p.momentumA < .947
				THEN 1
				ELSE
				CASE WHEN 
				p.closePrice >= (highFRLLineLong * .96)
				and p.momentumA > .96
				and (LAG(p.ActualChange, 1) OVER (ORDER BY p.tradeDate) - p.ActualChange) < 0.1
					THEN 0
					ELSE 4
				END
			END
		ELSE -- closePrice is < 100.00
			CASE WHEN 
			p.closePrice <= (medFRLLineLong * .98)
			and p.momentumA < .97
				THEN 1
				ELSE
				CASE WHEN 
				p.closePrice >= (HighFRLLineLong * .93)
				and p.momentumA > 1.02
				and (LAG(p.ActualChange, 1) OVER (ORDER BY p.tradeDate) - p.ActualChange) < 0.1
					THEN 0
					ELSE 4
				END
			END
		END -- END OF CASE FOR DOLLAR $$ AMOUNT
	END -- END OF CASE FOR DATE (6Months or Older)
END) AS FRLTradeAction,


-----------------------------------------------------
--1000 day
-----------------------------------------------------
(Case when dbo.fn_Get_TradeAction(p.closePrice,highFRLLineOneK,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	OR dbo.fn_Get_TradeAction(p.closePrice,medFRLLineOneK,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	or dbo.fn_Get_TradeAction(p.closePrice,lowFRLLineOneK,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	THEN 4
	ELSE
		CASE WHEN (p.tradeDate <= DATEADD(month, -6, GETDATE())) -- OLDER tradeDate than 6Months Ago
			THEN 
			CASE WHEN p.closePrice > 100.00 -- closePrice is Greater than 100.00
				THEN
				CASE WHEN p.closePrice <= (highFRLLineOneK * 1.01)
				and p.closePrice >= (highFRLLineOneK * .95)
					THEN 1
					ELSE
					CASE WHEN
					-- closePrice is higher than FRL, but not drastically
					p.closePrice >= (highFRLLineOneK * 1.005)
					and p.closePrice < (highFRLLineOneK * 1.04)
						THEN 0
						ELSE 4
					END
				END
			ELSE -- ELSE $$$$ < 100.00
			CASE WHEN p.closePrice <= (medFRLLineOneK * .95)
			and p.momentumA < 1.009
				THEN 1
				ELSE
				CASE WHEN
				-- closePrice is higher than FRL, but not drastically
				p.closePrice >= (highFRLLineOneK * .925)
				and p.momentumA >1.02
					THEN 0
					ELSE 4
				END
			END
		END
	ELSE -- ELSE NEWER than 6Mos
	CASE WHEN p.closeprice > 100.00
		THEN
		CASE WHEN p.closePrice <= (highFRLLineOneK * 1.03)
			and p.closePrice >= (highFRLLineOneK * 1.005)
				THEN 1
				ELSE
				CASE WHEN
				-- closePrice is higher than FRL, but not drastically
				p.closePrice >= (highFRLLineOneK * 1.075)
				and p.closePrice < (highFRLLineOneK * 1.025)
					THEN 0
					ELSE 4
				END
			END
		ELSE -- ELSE $$$$ < 100.00
			CASE WHEN 
			p.closePrice <= (medFRLLineOneK * .975)
			and p.momentumA < 1.0
			THEN 1
				ELSE
				CASE WHEN
				-- closePrice is Trending higher than FRL, but not drastically
				p.closePrice >= (highFRLLineOneK * .95)
				and p.momentumA > 1.04
					THEN 0
					ELSE 4
				END
			END
		END -- END OF DOLLAR $$$$
	END -- END OF DATE
END) AS FRL_1K_TradeAction

INTO dbo.PFE_Act
FROM dbo.PFE_Pred as p
JOIN dbo.PFE_TS as ts on ts.tradeDate = p.tradeDate
JOIN dbo.PFE_Pred2 as p2 on p2.tradeDate = p.tradeDate
ORDER BY p.tradeDate, p2.tradeDate, ts.tradeDate ASC

--------------------------- update fib columns PFE------------------
SELECT TOP 1 @fibExtLowNeg = fibExtLowNeg
    , @fibExtHighNeg = [fibExtHighNeg]
    , @fibExtLowPos = [fibExtLowPos] 
    ,@fibExtHighPos = [fibExtHighPos]
FROM dbo.PFE_Pred2 ORDER BY tradeDate DESC


UPDATE dbo.PFE_Pred2 SET [fibExtLowNeg] = @fibExtLowNeg , [fibExtHighNeg] = @fibExtHighNeg ,  fibExtHighPos = NULL, fibExtLowPos = NULL
--------------------------- update fib columns PFE------------------

END
