USE [GMFSP_db]
GO
/****** Object:  StoredProcedure [dbo].[sp_Pred]    Script Date: 12/10/2018 10:34:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-----------------------------------------------
-- Author: Chris Del Duco
-- Author: Mohammad Moubadder
--
--			STORED PROCEDURE:
--				sp_Pred
-----------------------------------------------

ALTER PROCEDURE [dbo].[sp_Pred]

AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


-----------------------------------------------
-- FINANCIAL INSTRUMENT:	S&P 500 ETF Trust (SPY)
-- COMPUTATIONS:			CMA
-- CREATES dbo.SPY_Pred from dbo.SPY
-----------------------------------------------
IF EXISTS (SELECT * 
	FROM GMFSP_db.INFORMATION_SCHEMA.TABLES   
	WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'SPY_Pred')
Drop table dbo.SPY_Pred
SELECT tick.tradeDate, tick.closePrice,

	(LAG(wCMA, 1) OVER (ORDER BY ts.tradeDate) / lCMA) as BUYweekApproach,
	(wCMA / lCMA) as week_Long,
	(LAG(wCMA, 1) OVER (ORDER BY ts.tradeDate) / sCMA) as SELLweekApproach,
	(wCMA / sCMA) as week_Short,
		
	--100 day FRL Lines
	lPeak - ((lPeak - lTrough) * 0.236) AS highFRLLineLong,
	lPeak - ((lPeak - lTrough) * 0.382) AS medFRLLineLong,
	lPeak - ((lPeak - lTrough) * 0.618) AS lowFRLLineLong,
	--1000 day FRL lines
	kPeak - ((kPeak - kTrough) * 0.236) AS highFRLLineOneK,
	kPeak - ((kPeak - kTrough) * 0.382) AS medFRLLineOneK,
	kPeak - ((kPeak - kTrough ) * 0.618) AS lowFRLLineOneK,
	
	closePrice / LAG(closePrice,1) OVER (ORDER BY tick.tradeDate) AS ActualChange,
	-- 5 day Momentum
	closePrice / LAG(closePrice,5) OVER (ORDER BY ts.tradeDate) AS momentumA,
	-- 10 day Momentum
	closePrice / LAG(closePrice,10) OVER (ORDER BY ts.tradeDate) AS momentumB,
	(closePrice - LAG(closePrice,5) OVER (ORDER BY ts.tradeDate)) / LAG(closePrice,5) OVER (ORDER BY ts.tradeDate) * 100 AS rateOfChange

INTO dbo.SPY_Pred
FROM dbo.SPY AS tick
JOIN dbo.SPY_TS as ts on ts.tradeDate = tick.tradeDate
ORDER BY tick.tradeDate, ts.tradeDate ASC

-----------------------------------------------
-- FINANCIAL INSTRUMENT:	S&P Pharm ETF (XPH)
-- COMPUTATIONS:			CMA
-- CREATES dbo.XPH_Pred from dbo.XPH
-----------------------------------------------
IF EXISTS (SELECT * 
	FROM GMFSP_db.INFORMATION_SCHEMA.TABLES   
	WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'XPH_Pred')
Drop table dbo.XPH_Pred
SELECT tick.tradeDate, tick.closePrice,
	
	(LAG(wCMA, 1) OVER (ORDER BY ts.tradeDate) / lCMA) as BUYweekApproach,
	(wCMA / lCMA) as week_Long,
	(LAG(wCMA, 1) OVER (ORDER BY ts.tradeDate) / sCMA) as SELLweekApproach,
	(wCMA / sCMA) as week_Short,
		
	--100 day FRL Lines
	lPeak - ((lPeak - lTrough) * 0.236) AS highFRLLineLong,
	lPeak - ((lPeak - lTrough) * 0.382) AS medFRLLineLong,
	lPeak - ((lPeak - lTrough) * 0.618) AS lowFRLLineLong,
	--1000 day FRL lines
	kPeak - ((kPeak - kTrough) * 0.236) AS highFRLLineOneK,
	kPeak - ((kPeak - kTrough) * 0.382) AS medFRLLineOneK,
	kPeak - ((kPeak - kTrough ) * 0.618) AS lowFRLLineOneK,
	
	closePrice / LAG(closePrice,1) OVER (ORDER BY tick.tradeDate) AS ActualChange,
	-- 5 day Momentum
	closePrice / LAG(closePrice,5) OVER (ORDER BY ts.tradeDate) AS momentumA,
	-- 10 day Momentum
	closePrice / LAG(closePrice,10) OVER (ORDER BY ts.tradeDate) AS momentumB,
	(closePrice - LAG(closePrice,5) OVER (ORDER BY ts.tradeDate)) / LAG(closePrice,5) OVER (ORDER BY ts.tradeDate) * 100 AS rateOfChange

INTO dbo.XPH_Pred
FROM dbo.xph AS tick
JOIN dbo.XPH_TS as ts on ts.tradeDate = tick.tradeDate
ORDER BY tick.tradeDate, ts.tradeDate ASC

----------------------------------------
-- FINANCIAL INSTRUMENT:	PFIZER (PFE)
-- COMPUTATIONS:			CMA
-- CREATES dbo.PFE_Pred from dbo.PFE
----------------------------------------	
IF EXISTS (SELECT * 
	FROM GMFSP_db.INFORMATION_SCHEMA.TABLES   
	WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'PFE_Pred')
Drop table dbo.PFE_Pred
SELECT tick.tradeDate, tick.closePrice,
	
	(LAG(wCMA, 1) OVER (ORDER BY ts.tradeDate) / lCMA) as BUYweekApproach,
	(wCMA / lCMA) as week_Long,
	(LAG(wCMA, 1) OVER (ORDER BY ts.tradeDate) / sCMA) as SELLweekApproach,
	(wCMA / sCMA) as week_Short,
		
	--100 day FRL Lines
	lPeak - ((lPeak - lTrough) * 0.236) AS highFRLLineLong,
	lPeak - ((lPeak - lTrough) * 0.382) AS medFRLLineLong,
	lPeak - ((lPeak - lTrough) * 0.618) AS lowFRLLineLong,
	--1000 day FRL lines
	kPeak - ((kPeak - kTrough) * 0.236) AS highFRLLineOneK,
	kPeak - ((kPeak - kTrough) * 0.382) AS medFRLLineOneK,
	kPeak - ((kPeak - kTrough ) * 0.618) AS lowFRLLineOneK,
	
	closePrice / LAG(closePrice,1) OVER (ORDER BY tick.tradeDate) AS ActualChange,
	-- 5 day Momentum
	closePrice / LAG(closePrice,5) OVER (ORDER BY ts.tradeDate) AS momentumA,
	-- 10 day Momentum
	closePrice / LAG(closePrice,10) OVER (ORDER BY ts.tradeDate) AS momentumB,
	(closePrice - LAG(closePrice,5) OVER (ORDER BY ts.tradeDate)) / LAG(closePrice,5) OVER (ORDER BY ts.tradeDate) * 100 AS rateOfChange

INTO dbo.PFE_Pred
FROM dbo.PFE AS tick
JOIN dbo.PFE_TS as ts on ts.tradeDate = tick.tradeDate
ORDER BY tick.tradeDate, ts.tradeDate ASC


BEGIN
	exec dbo.sp_Pred2;
END

END
