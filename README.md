Clone:
- Open Project Folder (ie: C:\Users\YourName\PycharmProjects)
- Right click and open GitBash here
- type: git clone https://gitlab.com/GMFinTech/project.git
- Open PyCharm and Open Project

Comitting Code:
- Initiate Pull Request BEFORE YOU START TO CODE
    - This will ensure your code base is up to date
- Commit Code
    - Add comment to explain what you updated
- Push Code
    - Push code to Development Branch
- If code is ready to merge with master
    - Create Merge Request
    - Notify Team on Slack
    - Team Lead or 1 other team mate will review code
    - COMMENT YOUR MERGE REQUESTS
    - Merge with Master

**UPDATE TEAM ON ALL MERGES IN SLACK**